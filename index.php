<?php

ini_set('max_execution_time', 300);
include 'ntpServers.php';

foreach ($ntpServers as $value) {
    $timeserver = $value;
    $timercvd = query_time_server($timeserver, 37);
    if (!$timercvd[1]) {
        $timevalue = bin2hex($timercvd[0]);
        $timevalue = abs(HexDec('7fffffff') - HexDec($timevalue) - HexDec('7fffffff'));
        $tmestamp = $timevalue - 2208988800;
        $date = date("Y-m-d (D) H:i:s", $tmestamp - date("Z", $tmestamp) + 10800);
        echo "Moscow time from time server ", $timeserver, "<p>";
        echo $date;
        break;
    }
}

function query_time_server($timeserver, $socket) {

    $fp = fsockopen($timeserver, $socket, $err);
    if ($fp) {
        fputs($fp, "\n");
        $timevalue = fread($fp, 49);
        fclose($fp);
    }

    $ret = array();
    echo'<p>';
    $ret[] = $timevalue;
    $ret[] = $err;
    return($ret);
}
