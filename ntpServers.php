<?php

$ntpServers = array(
    'ptbtime1.ptb.de',
    'ptbtime2.ptb.de',
    'ntp0.fau.de',
    "ntps1-0.cs.tu-berlin.de",
    "ntps1-1.cs.tu-berlin.de",
    "ntps1-0.uni-erlangen.de",
    "ntp-p1.obspm.fr",
    "time.ien.it",
    "time-a.timefreq.bldrdoc.gov",
    "time-b.timefreq.bldrdoc.gov",
    "time-c.timefreq.bldrdoc.gov",
    "tick.usno.navy.mil",
    "tock.usno.navy.mil",
    "ntp2.usno.navy.mil");
